# Simple login/data webpage

## Get started
**For windows, assuming that you have git & yarn installed**

1. Open powershell and use `cd "the directory where you want to copy the app to"`
2. `git clone https://gitlab.com/demonpiontb/login-contacts-webpage.git`
3. `cd ./login-contacts-webpage` to move to app dir
4. `yarn` to add yarn scripts
5. `yarn start` to launch app
**Note: two minimized instances of powershell would be launched. Close them with ^C to stop the app.**
6. Use "testuser" as login and "password" as password to reach the data page. (without brackets)