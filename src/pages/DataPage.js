import React, { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { AuthContext } from '../utils/context'
import contacts from '../utils/contacts'
import DataTable from 'react-data-table-component';
import Popup from 'reactjs-popup';

export default function DataPage() {
    const {isAuthorized, setIsAuthorized} = useContext(AuthContext);
    const [pending, setPending] = useState(true);
    const [data, setData] = useState({data: [], length: 0}); //Length property added for force rerender, as it was not working sometimes
    const [filteredData, setFilteredData] = useState(false);
    const [editID, setEditID] = useState(false);
    const [editName, setEditName] = useState('');
    const [editNumber, setEditNumber] = useState('');
    const [openEdit, setOpenEdit] = useState(false);
    const [openAdd, setOpenAdd] = useState(false);
    const [searchText, setSearchText] = useState('');
    const [searchBy, setSearchBy] = useState('name');
    const addName = React.createRef();
    const addNumber = React.createRef();
    const closeModalEdit = () => setOpenEdit(false);
    const closeModalAdd = () => setOpenAdd(false);
    const navigate = useNavigate();

    // SHRINK BLOCK UNDER
    const columns = [
        {
            name: 'Name',
            selector: row => row.name,
        },
        {
            name: 'Number',
            selector: row => row.number,
        },
        {
            name: '',
            cell: row => (
                <button className='datapage-button'
                  onClick={() => deleteContact(row.id)}
                >  
                  DELETE
                </button>
              ),
            maxWidth: '70px'
        },
        {
            name: '',
            cell: row => (
                <button className='datapage-button'
                  onClick={() => openAndFillEditPopup(row.id)}
                >  
                  EDIT
                </button>
              ),
            maxWidth: '70px'
        }
    ];
    // SHRINK BLOCK UNDER
    const customStyles = {
        rows: {
            style: {
                backgroundColor: '#FAE7B5',
            },
        },
        headCells: {
            style: {
                backgroundColor:'#FAE797',
                fontWeight: 'bold',
            },
        },
    };

    const logOut = () => {
        setIsAuthorized(false);
    }

    const getAllData = async() => {
        let data = await contacts.getContacts();
        setPending(false);
        return data
    }

    const refreshData = async() => {
        const newData = await getAllData();
        setData({data:newData, length: newData.length});
    }

    const deleteContact = async(id) => {
        await contacts.deleteContact(id);
        refreshData();
    }

    const openAndFillEditPopup = async(id) => {
        const contactData = await contacts.getContactByID(id);
        setEditName(contactData.name);
        setEditNumber(contactData.number);
        setEditID(id);
        setOpenEdit(o => !o);
    }

    const editContact = async(e) => {
        e.preventDefault();
        await contacts.editContact(editID, {name:editName, number:editNumber});
        closeModalEdit();
        refreshData();
    }

    const handleEditName = (e) => {
        setEditName(e.target.value);
    }

    const handleEditNumber = (e) => {
        setEditNumber(e.target.value);
    }

    const addContact = async(e) => {
        e.preventDefault();
        await contacts.addContact({name: addName.current.value, number: addNumber.current.value});
        closeModalAdd();
        refreshData();
    }

    const handleSearchSubmit = (e) => {
        e.preventDefault();
    }

    const handleSearchByChange = (e) => {
        setSearchBy(e.target.value);
    }

    const handleSearchTextChange = (e) => {
        if (e) {
            setSearchText(e.target.value);
        }
        const localSearchText = e ? e.target.value : searchText;
        if (localSearchText !== ''){
            const tempDataArray = [];
            const len = localSearchText.length;
            switch (searchBy) {
                case "name":
                    data.data.forEach((element) => {
                        if (element.name.toLowerCase().slice(0, len) === localSearchText.toLowerCase()) {
                            tempDataArray.push(element);
                        }
                    })
                    break;
                case "number":
                    data.data.forEach((element) => {
                        if (element.number.toLowerCase().slice(0, len) === localSearchText.toLowerCase()) {
                            tempDataArray.push(element);
                        }
                    })
                    break;
            }
            setFilteredData(tempDataArray);
        } else {
            clearFilters();
        }
    }

    const clearFilters = () => {
        setSearchText('');
        setFilteredData(false);
    }

    useEffect(() => {
        if(!isAuthorized){
          navigate("/login");
        }
    },);

    useEffect(() => {
        refreshData();
    },[])

    useEffect(() => {
        if (searchText !== ''){
            handleSearchTextChange();
        }
    },[data.data])

    return (
        <div className='data-page'>
            <button className='logout-button' onClick={logOut}>LOGOUT</button>
            <form className='search-form' onSubmit={handleSearchSubmit}>
                <select name="searchBy" onChange={handleSearchByChange}>
                    <option value="name">Name</option>
                    <option value="number">Number</option>
                </select>
                <input type="text" name="search" value={searchText} onChange={handleSearchTextChange} />
                <button className='datapage-button' onClick={clearFilters}>CLEAR SEARCH</button>
            </form>
            <div className='table-wrapper'>
                <DataTable
                    columns={columns}
                    data={filteredData ? filteredData : data.data}
                    progressPending={pending}
                    customStyles={customStyles}
                />
                <button className='add-button' onClick={() => {setOpenAdd(o => !o)}}>ADD CONTACT</button>
            </div>

            <div>
                <Popup open={openEdit} closeOnDocumentClick onClose={closeModalEdit}>
                    <button className="close" onClick={() => {closeModalEdit()}}>CLOSE</button>
                    <div style={{fontWeight: 'bold'}}>Editing "{editName}":</div>
                    <form className='popup-form' onSubmit={editContact}>
                        <>Name:</>
                        <input type="text" name="name" placeholder="Name" onChange={handleEditName} value={editName} />
                        <>Number:</>
                        <input type="text" name="number" placeholder="Number" onChange={handleEditNumber} value={editNumber} />
                        <button type='submit'>Confirm changes</button>
                    </form>
                </Popup>

                <Popup open={openAdd} closeOnDocumentClick onClose={closeModalAdd}>
                    <button className="close" onClick={() => {closeModalAdd()}}>CLOSE</button>
                    <div style={{fontWeight: 'bold'}}>Add contact:</div>
                    <form className='popup-form' onSubmit={addContact}>
                        <input type="text" name="name" placeholder="Name" ref={addName} />
                        <input type="text" name="number" placeholder="Number" ref={addNumber} />
                        <button type='submit'>Add contact</button>
                    </form>
                </Popup>
            </div>
        </div>
    )
}

