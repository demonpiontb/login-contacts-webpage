import React, { useContext, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import users from '../utils/users'
import { AuthContext } from '../utils/context'

export default function LoginPage() {
    const {isAuthorized, setIsAuthorized} = useContext(AuthContext);
    const login = React.createRef();
    const password = React.createRef();
    const navigate = useNavigate();

    const authCheck = async (e) => {
        e.preventDefault();
        const data = await users.getUsers();
        data.forEach((element) => {
            if (login.current.value === element.login && password.current.value === element.password) {
                setIsAuthorized(true);
            }
        })
    }

    useEffect(() => {
        if(isAuthorized){
          navigate("/data");
        }
    },);

    return (
        <div className='login-wrapper'>
            <form className='login-form' onSubmit={authCheck}>
                Login <br />
                <input type='text' ref={login} name='login' required></input>
                Password <br />
                <input type='password' ref={password} name='password' required></input>
                <button type='submit'>Login</button>
            </form>
        </div>
    )
}
