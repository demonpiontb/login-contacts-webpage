const axios = require('axios');

const contacts = {
    getContacts: async() => {
        const { data } = await axios.get('http://localhost:3001/contacts');
        return data
    },
    getContactByID: async(id) => {
        const { data } = await axios.get(`http://localhost:3001/contacts/${id}/`);
        return data
    },
    editContact: async(id, edited) => {
        await axios.put(`http://localhost:3001/contacts/${id}/`, {
            name: edited.name,
            number: edited.number
        });
    },
    deleteContact: async(id) => {
        await axios.delete(`http://localhost:3001/contacts/${id}/`);
    },
    addContact: async(contact) => {
        axios.post('http://localhost:3001/contacts/', {
            id: contact.id,
            name: contact.name,
            number: contact.number
        });
    },
}

export default contacts;