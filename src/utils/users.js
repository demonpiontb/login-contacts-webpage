const axios = require('axios');

const users = {
    getUsers: async() => {
        const { data } = await axios.get('http://localhost:3001/users');
        return data
    }
}

export default users;

