import LoginPage from './pages/LoginPage'
import DataPage from './pages/DataPage'
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { useState } from 'react';
import { AuthContext } from './utils/context'

function App() {
  const [isAuthorized, setIsAuthorized] = useState(false)
  return (
    <AuthContext.Provider value={{isAuthorized, setIsAuthorized}}>
      <Router>
        <Routes>
          <Route path="/" element={<LoginPage />}></Route>
          <Route path="/login" element={<LoginPage />}></Route>
          <Route path="/data" element={<DataPage />}></Route>
        </Routes>
      </Router>
      </AuthContext.Provider>
  );
}

export default App;
